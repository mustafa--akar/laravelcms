
@extends('panel.layouts.master')

@section('content')



    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">

				<div class="row">
					<div class="col-md-12">
						<div class="widget">
							<header class="widget-header">
								<h4 class="widget-title">Dropzone</h4>
							</header><!-- .widget-header -->
							<hr class="widget-separator">
							<div class="widget-body">
								<form 
									data-url="/movies/refresh-image-list/{{ $movie->id }}"
									data-token="{{ csrf_token() }}"
									action="/movies/movie-image-store/{{ $movie->id  }}"
									id="dropzone" class="dropzone" data-plugin="dropzone" 
									data-options="{ url: '/movies/movie-image-store/{{ $movie->id }}'}">
									@csrf
									<div class="dz-message">
										<h3 class="m-h-lg">{{ __('Yüklemek istediğiniz resimleri buraya sürükleyerek bırakınız') }}</h3>
										<p class="m-b-lg text-muted">({{ __('Resimler otomatik olarak eklenecektir...') }})</p>
									</div>
								</form>
							</div><!-- .widget-body -->
						</div><!-- .widget -->
					</div><!-- END column -->
				</div>

				<div class="row">
					<div class="col-md-12">
						<h4 class="m-b-lg">
							<b>{{ $movie->title }}</b> {{ __('kaydına ait Resimler') }}			
						</h4>
					</div><!-- END column -->
					<div class="col-md-12">
						<div class="widget">			
							<div class="widget-body image_list_container">
								



			
								@empty($movieImages)

									<div class="alert alert-info">						
										<p>{{ __('Burada herhangi bir resim bulunmamaktadır') }}. </p>
									</div>	
								@else				
								

									<table class="table table-bordered table-striped table-hover pictures-list">
										<thead>
											<th class="order"><i class="fa fa-reorder"></i></th>
											<th>#id</th>
											<th>{{ __('Göresel') }}</th>											
											<th>{{ __('Durumu') }}</th>
											<th>{{ __('Kapak Fotoğrafı') }}</th>
											<th>{{ __('İşlem') }}</th>
										</thead>
										<tbody class="sortable" data-url="/movies/image-rank-setters" data-token="{{ csrf_token() }}">
											@foreach($movieImages as $image)
									
												<tr id="ord-{{ $image->id }}">
													<td class="order"><i class="fa fa-reorder"></i></td>
													<td class="text-center"># {{ $image->id }}</td>

		                                            @isset($image->img_url)
		                                                <td class="text-center">
		                                                    <a href="{{ asset('img/movies/') }}/{{ $image->img_url }}_big.jpg" data-lightbox="{{ $image->id }}" data-title="{{ $movie->title }}">
		                                                        <img src="{{ asset('img/movies/') }}/{{ $image->img_url }}_thumb.jpg" width="75" alt="" class="img-rounded">
		                                                    </a>

		                                                </td>
		                                            @endisset													

													
													<td class="text-center">
														<input 
															class="isActive"
															type="checkbox"
		                                                    data-url = "/movies/image-is-active-setters/{{ $movie->id }}"
	                                                   	    data-token = "{{ csrf_token() }}"
	                                                   	    data-id = "{{ $image->id }}"
															data-switchery
															data-color="#10c469"
															{{ ($image->isActive) ? "checked" : "" }}
														 />						
													</td>
													<td class="w100 text-center">
														<input 
															class="isCover"
															type="checkbox"
		                                                    data-url = "/movies/image-is-cover-setters/{{ $movie->id }}"
		                                                    data-id = "{{ $image->id }}"		                                                    
	                                                   	    data-token = "{{ csrf_token() }}"
															data-switchery
															data-color="#ff5b5b"
															{{ ($image->isCover) ? "checked" : "" }}

														 />						
													</td>							
													<td class="w100 text-center">
														<button 
															data-url="/movies/delete-movie-image/{{ $image->id }}"
															data-token="{{ csrf_token() }}"
															class="btn btn-danger btn-outline remove-btn">
															<i class="fa fa-trash"></i> {{ __('Sil') }}
														</button>						
													</td>
												</tr>
											@endforeach
											
										</tbody>
									</table>
								@endempty
				



							</div><!-- .widget-body -->
						</div><!-- .widget -->
					</div><!-- END column -->
				</div>

            </section><!-- .app-content -->
        </div><!-- .wrap -->






        <!-- APP FOOTER -->
        <div class="wrap p-t-0">
            <footer class="app-footer">
                <div class="clearfix">

                    <div class="copyright pull-left">Tüm Hakları Saklıdır | Mustafa Akar 2020 &copy;</div>
                </div>
            </footer>
        </div>
        <!-- /#app-footer -->
    </main>
    <!--========== END app main -->

@endsection









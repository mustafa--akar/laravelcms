
@extends('panel.layouts.master')

@section('content')



    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-b-lg">
                            {{ __('Yeni Film Ekle') }}
                        </h4>
                    </div><!-- END column -->
                    <div class="col-md-12">
                        <div class="widget">
                            <div class="widget-body">

                                <form action="/movies" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label>{{ __('Seçiniz') }}</label>                                        
                                        <select name="movie_or_series" class="form-control">
                                            <option value="movie">Film</option>
                                            <option value="series">Dizi</option>                                     
                                        </select>                                        
                                    </div>                                                                      
                                   
                                    <div class="form-group">
                                        <input type="text" value="" name="tags" data-plugin="tagsinput" data-role="tagsinput" class="form-control" placeholder="{{ _("Tag ekleyebilirsiniz... ") }}" />
                                    </div>
                                                                       
                                    <div class="form-group">
                                        <label>{{ __('Başlık') }}</label>
                                        <input class="form-control" placeholder="" name="title">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Puan (100 üzerinden sadece sayı giriniz)') }}</label>
                                        <input class="form-control" type="number" placeholder="" name="evaluation">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Açıklama') }}</label>
                                        <textarea name="description" class="m-0" data-plugin="summernote" data-options="{height: 250}"></textarea>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-md btn-outline">{{ __('Kaydet') }}</button>
                                    <a href="" class="btn btn-md btn-danger btn-outline">{{ __('İptal') }}</a>
                                </form>


                            </div><!-- .widget-body -->
                        </div><!-- .widget -->
                    </div><!-- END column -->
                </div>
            </section><!-- .app-content -->
        </div><!-- .wrap -->






        <!-- APP FOOTER -->
        <div class="wrap p-t-0">
            <footer class="app-footer">
                <div class="clearfix">

                    <div class="copyright pull-left">Tüm Hakları Saklıdır | Mustafa Akar 2020 &copy;</div>
                </div>
            </footer>
        </div>
        <!-- /#app-footer -->
    </main>
    <!--========== END app main -->

@endsection






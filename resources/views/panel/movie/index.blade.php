
@extends('panel.layouts.master')

@section('content')



    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-b-lg">
                            {{ __('Film Listesi') }}
                            <a href="/movies/create" class="btn btn-outline btn-primary btn-xs pull-right"><i class="fa fa-plus"></i>  {{ __('Yeni Ekle') }}</a>

                        </h4>
                    </div><!-- END column -->
                    <div class="col-md-12">
                        <div class="widget p-lg">

                            @isset($success)
                                    <div class="alert alert-success">
                                        {!! $success !!}
                                    </div>
                            @endisset

                            @if($movies->count() > 0 )
                                <table class="table table-striped table-hover content_container table-bordered">
                                    <thead>
                                        <th class="text-center order"><i class="fa fa-reorder"></i></th>
                                        <th class="text-center">#id</th>
                                        <th class="text-center">{{ __('Adı') }}</th>
                                        <th class="text-center">{{ __('Puan') }}</th>
                                        <th class="text-center">{{ __('Durum') }}</th>
                                        <th class="text-center">{{ __('İşlem') }}</th>
                                    </thead>
                                    <tbody class="sortable ui-sortable" data-url="/movies/rank-setters" data-token="{{ csrf_token() }}">
                                    @foreach($movies as $movie)
                                        <tr id="ord-{{ $movie->id }}" class="ui-sortable-handle">
                                            <td class="text-center order">
                                                @if($movie->movie_or_series == "series")
                                                    <i class="fa fa-reorder"></i>
                                                @else
                                                    <i class="fa fa-film"></i>    
                                                @endif
                                            </td>
                                            <td class="w50 text-center">#{{ $movie->id }}</td>
                                            <td class="text-center">{{ $movie->title }}</td>
                                            @isset($movie->evaluation)
                                                <td class="text-center">
                                                    {{ $movie->evaluation }}

                                                </td>
                                            @endisset

                                            <td class="text-center">
                                                <input
                                                    class="isActive"
                                                    type="checkbox"
                                                    data-url = "/movies/is-active-setters/{{ $movie->id }}"
                                                    data-token = "{{ csrf_token() }}"
                                                    data-switchery
                                                    data-color="#10c469"
                                                {{ $movie->isActive ? 'checked' : '' }}
                                            </td>
                                            <td class="text-center">

                                                <form style="display: inline;" action="/movies/{{ $movie->id }}" method="post">
                                                    @csrf
                                                    @method('DELETE')

                                                    <input class="btn btn-sm btn-danger btn-outline remove-btn" type="submit" value=" {{ __('Sil') }}">
                                                </form>



                                                <a href="/movies/{{ $movie->id }}/edit" class="btn btn-sm btn-warning btn-outline"><i class="fa fa-pencil-square-o"></i> {{ __('Düzenle') }}</a>

                                                <a href="/movies/{{ $movie->id }}/images-create" class="btn btn-sm btn-dark btn-outline"><i class="fa fa-image"></i> {{ __('Resimler') }}</a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                    {{ $movies->links() }}
                            @else
                                <div class="alert alert-info">
                                    <h4 class="alert-title">{{ __('Kayıt Bulunamadı') }}</h4>
                                    <p>{{ __('Burada herhangi bir kayıt bulunmamaktadır. Eklemek için lütfen') }} <a href="/movies/create">{{ __('tıklayınız') }}</a> </p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

            </section><!-- .app-content -->
        </div><!-- .wrap -->






        <!-- APP FOOTER -->
        <div class="wrap p-t-0">
            <footer class="app-footer">
                <div class="clearfix">

                    <div class="copyright pull-left">Tüm Hakları Saklıdır | Mustafa Akar 2020 &copy;</div>
                </div>
            </footer>
        </div>
        <!-- /#app-footer -->
    </main>
    <!--========== END app main -->

@endsection


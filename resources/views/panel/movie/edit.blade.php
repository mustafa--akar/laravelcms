
@extends('panel.layouts.master')

@section('content')



    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-b-lg">
                            {{ $movie->title }} {{ __(' isimli içeriği düzenliyorsunuz') }}
                        </h4>
                    </div><!-- END column -->
                    <div class="col-md-12">
                        <div class="widget">
                            <div class="widget-body">

                                @if($errors->any())

                                    <div class="alert alert-danger">
                                        {{ _('Lütfen girdiğiniz değerleri kontrol edin ! ') }}
                                        <ul class="mb-0">
                                            @foreach($errors->all() as $error)
                                                <li>{!! $error !!}</li>
                                            @endforeach
                                        </ul>
                                    </div>

                                @endif

                                <form action="/movies/{{ $movie->id }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label>{{ __('Seçiniz') }}</label>                                        
                                        <select name="movie_or_series" class="form-control">
                                            <option {{ $movie->movie_or_series === "movie" ? "selected" : ""   }} value="movie">Film</option>
                                            <option {{ $movie->movie_or_series === "series" ? "selected" : ""   }} value="series">Dizi</option>                                     
                                        </select>                                        
                                    </div>  

                                    <div class="form-group">
                                    <input type="text" value="{{ $movie->tags ?? old('tags') }}" name="tags" data-plugin="tagsinput" data-role="tagsinput" class="form-control" placeholder="{{ _("Tag ekleyebilirsiniz... ") }}" />
                                    </div> 

                                    <div class="form-group">
                                        <label>{{ __('Başlık') }}</label>
                                        <input class="form-control" placeholder="" name="title" value="{{ $movie->title ?? old('title') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Puan') }}</label>
                                        <input class="form-control" placeholder="" name="evaluation" value="{{ $movie->evaluation ?? old('evaluation') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Açıklama') }}</label>
                                        <textarea name="description" class="m-0" data-plugin="summernote" data-options="{height: 250}">{{ $movie->description ?? old('description') }}</textarea>
                                    </div>



                                    <button type="submit" class="btn btn-primary btn-md btn-outline">{{ __('Kaydet') }}</button>
                                    <a href="" class="btn btn-md btn-danger btn-outline">{{ __('İptal') }}</a>
                                </form>
                            </div><!-- .widget-body -->
                        </div><!-- .widget -->
                    </div><!-- END column -->
                </div>
            </section><!-- .app-content -->
        </div><!-- .wrap -->






        <!-- APP FOOTER -->
        <div class="wrap p-t-0">
            <footer class="app-footer">
                <div class="clearfix">

                    <div class="copyright pull-left">Tüm Hakları Saklıdır | Mustafa Akar 2020 &copy;</div>
                </div>
            </footer>
        </div>
        <!-- /#app-footer -->
    </main>
    <!--========== END app main -->

@endsection






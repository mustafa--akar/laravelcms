			
					
												
							
								<table class="table table-bordered table-striped table-hover pictures-list">
									<thead>
										<th class="order"><i class="fa fa-reorder"></i></th>
										<th>#id</th>
										<th>Göresel</th>										
										<th>Durumu</th>
										<th>Kapak Fotoğrafı</th>
										<th>İşlem</th>
									</thead>
									<tbody class="sortable" data-url="/movies/image-rank-setters" data-token="{{ csrf_token() }}">
										@foreach($movieImages as $image)

											<tr id="ord-{{ $image->id }}">
												<td class="order"><i class="fa fa-reorder"></i></td>
												<td class="w100 text-center">#</td>
                                                <td class="text-center">
                                                    <a href="{{ asset('img/movies/') }}/{{ $image->img_url }}_big.jpg" data-lightbox="{{ $image->id }}" data-title="">
                                                        <img src="{{ asset('img/movies/') }}/{{ $image->img_url }}_thumb.jpg" width="75" alt="" class="img-rounded">
                                                    </a>

                                                </td>												
												<td class="w100 text-center">
													<input 
														class="isActive"
														type="checkbox"
	                                                    data-url = "/movies/image-is-active-setters/{{ $movie->id }}"
                                                   	    data-token = "{{ csrf_token() }}"
                                                   	    data-id = "{{ $image->id }}"
														data-switchery
														data-color="#10c469"
														{{ ($image->isActive) ? "checked" : "" }}
													 />						
												</td>
												<td class="w100 text-center">
													<input 
														class="isCover"
														type="checkbox"
	                                                    data-url = "/movies/image-is-cover-setters/{{ $movie->id }}"
                                                   	    data-token = "{{ csrf_token() }}"
                                                   	    data-id = "{{ $image->id }}"
														data-switchery
														data-color="#ff5b5b"
														{{ ($image->isCover) ? "checked" : "" }}
													 />						
												</td>							
												<td class="w100 text-center">
													<button 
														data-url="/movies/delete-movie-image/{{ $image->id }}"
														data-token="{{ csrf_token() }}"
														class="btn btn-danger btn-outline remove-btn">
														<i class="fa fa-trash"></i> {{ __('Sil') }}
													</button>						
												</td>
											</tr>
										@endforeach	
										
									</tbody>
								</table>
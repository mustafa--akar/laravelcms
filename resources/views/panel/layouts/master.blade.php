@include('panel.layouts.header')

@include('panel.layouts.navbar')

@include('panel.layouts.aside')

@yield('content')

@include('panel.layouts.script')
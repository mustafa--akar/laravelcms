<script>
    var LIBS = {
// Chart libraries
        plot: [
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.min.js",
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.pie.min.js",
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.stack.min.js",
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.resize.min.js",
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.curvedLines.js",
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.tooltip.min.js",
            "{{ asset('panel_template/') }}/libs/misc/flot/jquery.flot.categories.min.js"
        ],
        chart: [
            '{{ asset('panel_template/') }}/libs/misc/echarts/build/dist/echarts-all.js',
            '{{ asset('panel_template/') }}/libs/misc/echarts/build/dist/theme.js',
            '{{ asset('panel_template/') }}/libs/misc/echarts/build/dist/jquery.echarts.js'
        ],
        circleProgress: [
            "{{ asset('panel_template/') }}/libs/bower/jquery-circle-progress/dist/circle-progress.js"
        ],
        sparkline: [
            "{{ asset('panel_template/') }}/libs/misc/jquery.sparkline.min.js"
        ],
        maxlength: [
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-maxlength/src/bootstrap-maxlength.js"
        ],
        tagsinput: [
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.css",
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
        ],
        TouchSpin: [
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"
        ],
        selectpicker: [
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-select/dist/css/bootstrap-select.min.css",
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-select/dist/js/bootstrap-select.min.js"
        ],
        filestyle: [
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-filestyle/src/bootstrap-filestyle.min.js"
        ],
        timepicker: [
            "{{ asset('panel_template/') }}/libs/bower/bootstrap-timepicker/js/bootstrap-timepicker.js"
        ],
        datetimepicker: [
            "{{ asset('panel_template/') }}/libs/bower/moment/moment.js",
            "{{ asset('panel_template/') }}/libs/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
            "{{ asset('panel_template/') }}/libs/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"
        ],
        select2: [
            "{{ asset('panel_template/') }}/libs/bower/select2/dist/css/select2.min.css",
            "{{ asset('panel_template/') }}/libs/bower/select2/dist/js/select2.full.min.js"
        ],
        vectorMap: [
            "{{ asset('panel_template/') }}/libs/misc/jvectormap/jquery-jvectormap.css",
            "{{ asset('panel_template/') }}/libs/misc/jvectormap/jquery-jvectormap.min.js",
            "{{ asset('panel_template/') }}/libs/misc/jvectormap/maps/jquery-jvectormap-us-mill.js",
            "{{ asset('panel_template/') }}/libs/misc/jvectormap/maps/jquery-jvectormap-world-mill.js",
            "{{ asset('panel_template/') }}/libs/misc/jvectormap/maps/jquery-jvectormap-africa-mill.js"
        ],
        summernote: [
            "{{ asset('panel_template/') }}/libs/bower/summernote/dist/summernote.css",
            "{{ asset('panel_template/') }}/libs/bower/summernote/dist/summernote.min.js"
        ],
        DataTable: [
            "{{ asset('panel_template/') }}/libs/misc/datatables/datatables.min.css",
            "{{ asset('panel_template/') }}/libs/misc/datatables/datatables.min.js"
        ],
        fullCalendar: [
            "{{ asset('panel_template/') }}/libs/bower/moment/moment.js",
            "{{ asset('panel_template/') }}/libs/bower/fullcalendar/dist/fullcalendar.min.css",
            "{{ asset('panel_template/') }}/libs/bower/fullcalendar/dist/fullcalendar.min.js"
        ],
        dropzone: [
            "{{ asset('panel_template/') }}/libs/bower/dropzone/dist/min/dropzone.min.css",
            "{{ asset('panel_template/') }}/libs/bower/dropzone/dist/min/dropzone.min.js"
        ],
        counterUp: [
            "{{ asset('panel_template/') }}/libs/bower/waypoints/lib/jquery.waypoints.min.js",
            "{{ asset('panel_template/') }}/libs/bower/counterup/jquery.counterup.min.js"
        ],
        others: [
            "{{ asset('panel_template/') }}/libs/bower/switchery/dist/switchery.min.css",
            "{{ asset('panel_template/') }}/libs/bower/switchery/dist/switchery.min.js",
            "{{ asset('panel_template/') }}/libs/bower/lightbox2/dist/css/lightbox.min.css",
            "{{ asset('panel_template/') }}/libs/bower/lightbox2/dist/js/lightbox.min.js",
            "{{ asset('panel_template/') }}/assets/js/custom.js"

        ]
    };
</script>


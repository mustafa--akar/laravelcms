<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)"><img class="img-responsive" src="{{ asset('panel_template/assets/') }}/images/221.jpg" alt="avatar"/></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username">{{ auth()->user()->name }}</a></h5>
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <small>Netflixer</small>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu animated flipInY">
                <li>
                  <a class="text-color" href="">
                    <span class="m-r-xs"><i class="fa fa-home"></i></span>
                    <span>{{ __('Ana Sayfa') }}</span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="">
                    <span class="m-r-xs"><i class="fa fa-user"></i></span>
                    <span>{{ __('Profilim') }}</span>
                  </a>
                </li>

                <li role="separator" class="divider"></li>
                <li>
                    <a class="text-color" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                        <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                        <span>{{ __('Çıkış Yap') }}</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">

        <li class="{{ Request::is('home*') ? 'active' : '' }}">
          <a href="javascript:void(0)">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">{{ __('Ana Sayfa') }}</span>
          </a>
        </li>
        <li class="{{ Request::is('settings*') ? 'active' : '' }}">
          <a href="/settings">
            <i class="menu-icon zmdi zmdi-settings zmdi-hc-lg"></i>
            <span class="menu-text">{{ __('Site Ayarları') }}</span>
          </a>
        </li>
        <li class="{{ Request::is('sliders*') ? 'active' : '' }}">
          <a href="/sliders">
            <i class="menu-icon zmdi zmdi-layers zmdi-hc-lg"></i>
            <span class="menu-text">{{ __('Slider') }}</span>
          </a>
        </li>
          <li class="{{ Request::is('movies*') ? 'active' : '' }}">
              <a href="/movies">
                  <i class="menu-icon fa fa-video-camera"></i>
                  <span class="menu-text">{{ __('Filmler') }}</span>
              </a>
          </li>




        <li class="menu-separator"><hr></li>


      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->

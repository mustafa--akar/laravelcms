
@extends('panel.layouts.master')

@section('content')



    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="m-b-lg">
                            {{ $slider->title }} {{ __(' isimli içeriği düzenliyorsunuz') }}
                        </h4>
                    </div><!-- END column -->
                    <div class="col-md-12">
                        <div class="widget">
                            <div class="widget-body">

                                <form action="/sliders/{{ $slider->id }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label>{{ __('Başlık') }}</label>
                                        <input class="form-control" placeholder="" name="title" value="{{ $slider->title ?? old('title') }}">
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __('Açıklama') }}</label>
                                        <textarea name="description" class="m-0" data-plugin="summernote" data-options="{height: 250}">{{ $slider->description ?? old('description') }}</textarea>
                                    </div>

                                    <div class="form-group image_upload_container">
                                        <div class="row">
                                            <div class="col-md-1">
                                                @if(file_exists("img/sliders/". $slider->img_url. "_thumb.jpg"))
                                                    <label>{{ __('Mevcut Görsel') }}</label>
                                                    <a href="{{ asset('img/sliders/') }}/{{ $slider->img_url }}_big.jpg" data-lightbox="{{ $slider->id }}" data-title="{{ $slider->title }}">
                                                        <img src="{{ asset('img/sliders/') }}/{{ $slider->img_url }}_thumb.jpg" width="75" alt="" class="img-rounded">
                                                    </a>

                                                @endif
                                            </div>
                                            <div class="col-md-11">
                                                <label>{{ __('Görsel Seçiniz') }}</label>
                                                <input type="file" name="img_url" class="form-control">
                                            </div>
                                        </div>


                                    </div>



                                    <button type="submit" class="btn btn-primary btn-md btn-outline">{{ __('Kaydet') }}</button>
                                    <a href="" class="btn btn-md btn-danger btn-outline">{{ __('İptal') }}</a>
                                </form>
                            </div><!-- .widget-body -->
                        </div><!-- .widget -->
                    </div><!-- END column -->
                </div>
            </section><!-- .app-content -->
        </div><!-- .wrap -->






        <!-- APP FOOTER -->
        <div class="wrap p-t-0">
            <footer class="app-footer">
                <div class="clearfix">

                    <div class="copyright pull-left">Tüm Hakları Saklıdır | Mustafa Akar 2020 &copy;</div>
                </div>
            </footer>
        </div>
        <!-- /#app-footer -->
    </main>
    <!--========== END app main -->

@endsection




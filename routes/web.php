<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('sliders', 'SliderController');

Route::post('/movies/movie-image-store/{movie}', 'MovieController@movieImageStore')->name('movieImageStore');

Route::post('/movies/refresh-image-list/{movie}', 'MovieController@refreshImageList')->name('refreshImageList');

Route::get('/movies/{movie}/images-create', 'MovieController@moviesImagesCreate');

Route::resource('movies', 'MovieController');

Route::post('/sliders/is-active-setters/{slider}', 'SliderController@isActiveSetter')->name('slidersIsActiveSetter');

Route::post('/sliders/rank-setters', 'SliderController@rankSetter')->name('slidersRankSetter');

Route::post('/movies/is-active-setters/{movie}', 'MovieController@isActiveSetter')->name('moviesIsActiveSetter');

Route::post('/movies/rank-setters', 'MovieController@rankSetter')->name('moviesRankSetter');

Route::post('/movies/delete-movie-image/{image}', 'MovieController@deleteMovieImage')->name('deleteMovieImage');

Route::post('/movies/image-is-active-setters/{image}', 'MovieController@imageIsActiveSetter')->name('imageIsActiveSetter');

Route::post('/movies/image-is-cover-setters/{image}', 'MovieController@imageIsCoverSetter')->name('imageIsCoverSetter');

Route::post('/movies/image-rank-setters', 'MovieController@imageRankSetter')->name('imageRankSetter');

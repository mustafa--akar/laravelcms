$(document).ready(function(){

		$(".sortable").sortable();

		/**************** Sweet Alert und Image Delete  ***************************************/
		/**************************************************************************************/
		$(".content_container, .image_list_container").on('click', '.remove-btn', function(e){

			//e.preventDefault();    ---> link olsaydı bunu kullanarak href e gitmesini engelleyebilirdik

			var $data_url = $(this).data("url");
			var $data_id  = $(this).data("id");
			var $token    = $(this).data("token");


			Swal.fire({
			  title: 'Emin misiniz?',
			  text: "Bu işlemi geri alamayacaksınız!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#dd3333',
			  confirmButtonText: 'Evet, Sil!',
			  cancelButtonText:  'Hayır'
			}).then(function(result){
			  if (result.value) {
			  	$.post($data_url, { _token : $token, id : $data_id}, function(response){
			  		location.reload();
			  	});
				
			  }
			})

		});
		/***************************************************************************/
		/***************************************************************************/


		/***************** Is Active Setter Funktion *******************************/
		/***************************************************************************/

		$(".content_container, .image_list_container").on('change', '.isActive', function(){

			if($(this).data("id")){
				var $id = $(this).data("id");
			}

			var $data      = $(this).prop("checked");
			var $data_url  = $(this).data("url");
			var $token     = $(this).data('token');

			if(typeof $data !== "undefined" && typeof $data_url !== "undefined")
			{
				if($id){
					$.post($data_url, { data : $data, _token : $token, id : $id }, function(response){

					});
				}else{
	                $.post($data_url, { data : $data, _token : $token }, function(response){

					});					
				}

			}else{

			}
		});
		/**************************************************************************************/
		/**************************************************************************************/


		/******************** Is Cover Setter Funktion ****************************************/
		/**************************************************************************************/		
		$(".image_list_container").on('change', '.isCover', function(){
		
			
			var $id = $(this).data("id");
			var $data = $(this).prop("checked");
			var $data_url = $(this).data("url");			
			var $token     = $(this).data('token');

			if(typeof $data !== "undefined" && typeof $data_url !== "undefined")
			{
				
				$.post($data_url, { data : $data, _token : $token, id : $id }, function(response){
					$(".image_list_container").html(response);
					// Switchery Pluginini initialize ettik
					switch_initialize();
					// sortable yani sürükle bırak özelliğini tekrar çağırdık
					$(".sortable").sortable();
				});								
				

			}else{

			}
		});

		/*******************************************************************************************/
		/*******************************************************************************************/


		$(".content_container, .image_list_container").on("sortupdate", ".sortable", function(event, ui)
		{

			var $data = $(this).sortable("serialize");
            var $data_token = $(this).data('token');
			var $data_url = $(this).data("url");

			$.post($data_url, {data : $data, _token : $data_token}, function(response){

			});
		});


		$(".button-sldr-toggle").change(function(){

			$(".button-information-container").slideToggle();
		});

		$(".panel-footer").on("click", ".clear-all-input", function(){
			$("form.cms-email input,textarea").val("");
		});

		var uploadSection = Dropzone.forElement("#dropzone");

		uploadSection.on("complete", function(file){

			/*  file ile birlikte yeni yüklenen dosyanın adı da dahil olmak üzere bir çok parametreyi alabiliriz
			console.log(file);  */


			var $data_url = $("#dropzone").data("url");
			var $data_token = $("#dropzone").data("token");

			
			
			$.post($data_url, {_token : $data_token},function(response){
				
				$(".image_list_container").html(response);

				// Switchery Pluginini initialize ettik
				switch_initialize();
				// sortable yani sürükle bırak özelliğini tekrar çağırdık
				$(".sortable").sortable();

			});

		});

		function switch_initialize() {
			$('[data-switchery]').each(function(){
				var $this = $(this),
						color = $this.attr('data-color') || '#188ae2',
						jackColor = $this.attr('data-jackColor') || '#ffffff',
						size = $this.attr('data-size') || 'default'

				new Switchery(this, {
					color: color,
					size: size,
					jackColor: jackColor
				});
			});
		}


})

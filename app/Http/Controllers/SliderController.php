<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $success = Session::get('success');
        $sliders = Slider::orderBy('rank', 'ASC')->paginate(10);

        return view('panel.slider.index')->with(
            [
                'sliders'   => $sliders,
                'success'   => $success
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate(
            [
                'title'          => 'required|min:3',
                'description'    => 'required|min:5',
                'img_url'        => 'mimes:jpeg,jpg,png,bmp,gif'
            ]
        );

        $slider = new Slider(
            [
                'title'          => $request->title,
                'description'    => $request->description,
                'img_url'        => Str::slug($request->title).'-'.time()
            ]
        );

        $slider->save();

        $this->saveImage($request->img_url, $slider->img_url);

        return redirect('/sliders')->with('success', 'Kayıt başarılı bir şekilde eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        return view('panel.slider.edit')->with('slider', $slider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate(
            [
                'title'           => 'required|min:3',
                'description'     => 'required|min:3',
                'img_url'         => 'mimes:jpeg,jpg,png,bmp,gif'
            ]
        );
        $old_img_url = $slider->img_url;


        if($request->img_url){

            $slider->update(
                [
                    'title'           => $request->title,
                    'description'     => $request->description,
                    'img_url'         => Str::slug($request->title).'-'.time()
                ]
            );

            $this->saveImage($request->img_url, $slider->img_url);

            if(file_exists(public_path() . '/img/sliders/'  . $old_img_url . '_big.jpg')){
                unlink(public_path() . '/img/sliders/'  . $old_img_url . '_big.jpg');
            }
            if(file_exists(public_path() . '/img/sliders/'  . $old_img_url . '_thumb.jpg')){
                unlink(public_path() . '/img/sliders/'  . $old_img_url . '_thumb.jpg');
            }

        }else{
            $slider->update(
                [
                    'title'           => $request->title,
                    'description'     => $request->description
                ]
            );
        }



        return $this->index()->with(
            [
                'success'   => '<b>' . $request->title . '</b> isimli kayıt başarılı bir şekilde düzenlendi'
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $old_name = $slider->title;
        $slider->delete();

        $this->deleteImage($slider->img_url);

        return back()->with(
          [
              'success'     => '<b>' .$old_name. '</b> isimli kayıt başarılı bir şekilde silindi'
          ]
        );
    }

    /**
     * Updates the active and passive status of data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function isActiveSetter(Request $request, Slider $slider)
    {
        $isActive = ($request->data === "true") ? 1 : 0;

        $slider->update(
            [
                'isActive'           => $isActive
            ]
        );

    }

    /**
     * Sets ranking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */

    public function rankSetter(Request $request, Slider $slider)
    {
        parse_str($request->data, $order);
        $items = $order["ord"];

        foreach($items as $rank => $id)
        {
            $slider->where('id', $id)->where('rank', '!=', $rank)
                ->update(
                [
                    'rank'           => $rank
                ]
            );
        }
    }

    /**
     * Save Image.
     *
     * @param  $bildInput
     * @param  $img_url
     */
    public function saveImage($bildInput, $img_url)
    {
        Image::make($bildInput)
            ->widen(1200)
            ->save(public_path() . '/img/sliders/' .$img_url. '_big.jpg');

        Image::make($bildInput)
            ->widen(75)
            ->save(public_path() . '/img/sliders/'  . $img_url . '_thumb.jpg');
    }

    /**
     * Remove Image.
     *
     * @param  $old_img_url
     */
    public function deleteImage($old_img_url)
    {

        if(file_exists(public_path() . '/img/sliders/'  . $old_img_url . '_big.jpg')){
            unlink(public_path() . '/img/sliders/'  . $old_img_url . '_big.jpg');
        }
        if(file_exists(public_path() . '/img/sliders/'  . $old_img_url . '_thumb.jpg')){
            unlink(public_path() . '/img/sliders/'  . $old_img_url . '_thumb.jpg');
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Movie;
use App\MovieImage;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $success = Session::get('success');
        $movies = Movie::orderBy('rank', 'ASC')->paginate(10);

        return view('panel.movie.index')->with(
            [
                'movies'    => $movies,
                'success'   => $success
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.movie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'          => 'required|min:3',
                'description'    => 'required|min:5',
                'evaluation'     => 'required|numeric|max:100'
            ]
        );

        $movie = new Movie(
            [
                'title'           => $request->title,
                'description'     => $request->description,
                'movie_or_series' => $request->movie_or_series,
                'tags'            => $request->tags,  
                'evaluation'      => $request->evaluation
            ]
        );

        $movie->save();

        return redirect('/movies')->with('success', 'Kayıt başarılı bir şekilde eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {

        return view('panel.movie.edit')->with( [ "movie"   => $movie  ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $request->validate(
            [
                'title'           => 'required|min:3',
                'description'     => 'required|min:3',
                'evaluation'      => 'required|numeric|max:100'
            ]
        );

        $movie->update(
            [
                'title'           => $request->title,
                'description'     => $request->description,
                'movie_or_series' => $request->movie_or_series,
                'tags'            => $request->tags,
                'evaluation'      => $request->evaluation
            ]
        );

        return $this->index()->with(
            [
                'success'   => '<b>' . $request->title . '</b> isimli kayıt başarılı bir şekilde düzenlendi'
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $old_name = $movie->title;
        $movie->delete();

        return back()->with(
            [
                'success'     => '<b>' .$old_name. '</b> isimli kayıt başarılı bir şekilde silindi'
            ]
        );
    }

    /**
     * Updates the active and passive status of data.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function isActiveSetter(Request $request, Movie $movie)
    {
        $isActive = ($request->data === "true") ? 1 : 0;

        $movie->update(
            [
                'isActive'           => $isActive
            ]
        );

    }

    /**
     * Sets ranking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */

    public function rankSetter(Request $request, Movie $movie)
    {
        parse_str($request->data, $order);
        $items = $order["ord"];

        foreach($items as $rank => $id)
        {
            $movie->where('id', $id)->where('rank', '!=', $rank)
                ->update(
                    [
                        'rank'           => $rank
                    ]
                );
        }
    }

        /**
     * Show the form for creating a new Image-resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function moviesImagesCreate(Movie $movie)
    {
        $movieImages = MovieImage::where('movie_id', $movie->id)->orderBy('rank', 'ASC')->get();

        return view('panel.movie.imageCreate')->with(
                [
                    'movie'         => $movie,
                    'movieImages'   => $movieImages
                ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function movieImageStore(Request $request, $id)
    {

        $fileName = $request->file->getClientOriginalName();        
       
        
        $movieImages = new MovieImage(
            [
                'isActive'       => 1,
                'isCover'        => 0,
                'rank'           => 0,
                'movie_id'       => $id,         
                'img_url'        => Str::slug($fileName).'-'.time()
            ]
        );

        $movieImages->save();

        $this->saveImage($request->file, $movieImages->img_url);

       // return redirect('/movies')->with('success', 'Kayıt başarılı bir şekilde eklendi');
    } 

    public function refreshImageList($id)
    {

        $movieImages = MovieImage::where('movie_id', $id)->get(); 
        $movie = Movie::where('id', $id)->first();

        $render_html = view('panel.movie.renderElements.imageList')->with([
            'movieImages'   => $movieImages,
            'movie'         => $movie
        ]);

        echo $render_html;
    }

    /**
     * Save Image.
     *
     * @param  $bildInput
     * @param  $img_url
     */
    public function saveImage($bildInput, $img_url)
    {
        Image::make($bildInput)
            ->widen(1200)
            ->save(public_path() . '/img/movies/' .$img_url. '_big.jpg');

        Image::make($bildInput)
            ->widen(75)
            ->save(public_path() . '/img/movies/'  . $img_url . '_thumb.jpg');
    }

    /**
     * Remove Image.
     *
     * @param  $image
     */
    public function deleteImage($image)
    {
        
        $old_img_url = $image->img_url;

        if(file_exists(public_path() . '/img/movies/'  . $old_img_url . '_big.jpg')){
            unlink(public_path() . '/img/movies/'  . $old_img_url . '_big.jpg');
        }
        if(file_exists(public_path() . '/img/movies/'  . $old_img_url . '_thumb.jpg')){
            unlink(public_path() . '/img/movies/'  . $old_img_url . '_thumb.jpg');
        }
    }

    /**
     * Remove Image in DB.
     *
     * @param  $img_id
     */
    public function deleteMovieImage($img_id)
    {
        $image  = MovieImage::where('id', $img_id)->first();
        
        $unlink = $this->deleteImage($image);
        
        $delete = MovieImage::where('id', $img_id)->delete();       

    }


    /**
     * Updates the active and passive status of Image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovieImage  $MovieImage
     * @return \Illuminate\Http\Response
     */
    public function ImageIsActiveSetter(Request $request)
    {        
        $isActive = ($request->data === "true") ? 1 : 0;

        $image = MovieImage::where('id', $request->id)->first();

        $image->update(
            [
                'isActive'           => $isActive
            ]
        );

    }

    /**
     * Updates the cover and passive status of Image.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovieImage  $MovieImage
     * @return \Illuminate\Http\Response
     */
    public function ImageIsCoverSetter(Request $request)
    {
        $isCover = ($request->data === "true") ? 1 : 0;

        $image = MovieImage::where('id', $request->id)->first();

        $image->update(
            [
                'isCover'           => $isCover
            ]
        );

        MovieImage::where('id', '!=', $request->id)->where('movie_id', $image->movie_id)->update(['isCover'  => 0 ]);

        $movieImages = MovieImage::where('movie_id', $image->movie_id)->orderBy('rank', 'ASC')->get();

        $movie = Movie::where('id', $image->movie_id)->first();  

        $render_html = view('panel.movie.renderElements.imageList')->with([
            'movieImages'   => $movieImages,
            'movie'         => $movie
        ]);

        echo $render_html;
    }

    /**
     * Sets ranking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovieImage  $MovieImage
     * @return \Illuminate\Http\Response
     */

    public function ImageRankSetter(Request $request, MovieImage $image)
    {
        parse_str($request->data, $order);
        $items = $order["ord"];

        foreach($items as $rank => $id)
        {
            $image->where('id', $id)->where('rank', '!=', $rank)
                ->update(
                    [
                        'rank'           => $rank
                    ]
                );
        }
    }



}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['title', 'description', 'evaluation', 'movie_or_series', 'tags', 'rank', 'isActive'];
}
